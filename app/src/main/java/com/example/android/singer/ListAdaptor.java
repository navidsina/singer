package com.example.android.singer;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by sina on 1/28/2017.
 */

public class ListAdaptor extends ArrayAdapter<Item> {
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;
        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_element, parent, false);
        }
        Item currentItem = getItem(position);

        TextView AlbumNameTextView = (TextView) listItemView.findViewById(R.id.albumName);
        AlbumNameTextView.setText(currentItem.GetAlbumName());
        TextView SongTitleTextView = (TextView) listItemView.findViewById(R.id.SongTitle);
        SongTitleTextView.setText(currentItem.GetSongTitle());
        ImageView ItemImageView = (ImageView) listItemView.findViewById(R.id.image);
        if(currentItem.GetAlbumThumbURL() == "")
            ItemImageView.setVisibility(View.GONE);
        else {
            try {
//                URL url = new URL(currentItem.GetAlbumThumbURL());
//                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
//                Drawable d = Drawable.createFromStream(in , "src");
//                ItemImageView.setImageDrawable(d);
//                ItemImageView.setVisibility(View.VISIBLE);
            } catch (Exception e)
            {
                Log.v("ListAdaptor", e.getMessage());
            }

        }
        if(currentItem.GetCurrentlyPlaying()){
            listItemView.setBackgroundColor(ContextCompat.getColor(getContext() ,R.color.playing));
        }



        return listItemView;
    }

    public ListAdaptor(Activity context, ArrayList<Item> Words, int[] playingPosition)
    {
        super(context, 0, Words);
        this.playingPosition = playingPosition;
    }

    int[] playingPosition;



}
