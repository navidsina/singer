package com.example.android.singer;

/**
 * Created by sina on 1/28/2017.
 */

public class Item {
    String AlbumName;
    String SongTitle;
    String VideoID;
    String AlbumThumbURL;
    boolean currentlyPlaying;

    Item(String AlbumName, String SongTitle, String VideoID, String AlbumThumbURL){

        this.AlbumName = AlbumName;
        this.SongTitle = SongTitle;
        this.VideoID = VideoID;
        this.AlbumThumbURL = AlbumThumbURL;
        this.currentlyPlaying = false;
    }

    public String GetAlbumName(){
        return AlbumName;
    }


    public String GetSongTitle(){
        return SongTitle;
    }

    public String GetVideoID(){
        return VideoID;
    }

    public String GetAlbumThumbURL(){
        return AlbumThumbURL;
    }

    public boolean GetCurrentlyPlaying() {return currentlyPlaying;}
    public void SetCurrentlyPlaying(boolean currentlyPlaying){this.currentlyPlaying = currentlyPlaying;}




}
