package com.example.android.singer;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.net.URLEncoder;
import java.util.ArrayList;

public class MainActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    public static final String API_KEY = "AIzaSyCnDXQ09rAcvuOdwcw4m0KMlRrUBKDa6Qc";

    //https://www.youtube.com/watch?v=<VIDEO_ID>
    public String VIDEO_ID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // attaching layout xml
        setContentView(R.layout.activity_main);

        // Initializing YouTube player view
        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);
        youTubePlayerView.initialize(API_KEY, this);
        //youTubePlayerView.setAlpha(0);
        Button playButton = (Button) findViewById(R.id.play);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerHandle.play();
                Log.v("play","play");
            }
        });

        Button pauseButton = (Button) findViewById(R.id.pause);
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playerHandle.pause();
                Log.v("pause","pause");
            }
        });

        seekBar = (SeekBar) findViewById(R.id.seekbar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progressChanged = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChanged = progress;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                playerHandle.seekToMillis(progressChanged * playerHandle.getDurationMillis() / 100);

            }
        });

        Button searchButton = (Button) findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.editText);
                SingerName = editText.getText().toString();
                FillTheList(SingerName);
            }
        });


    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult result) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer player, boolean wasRestored) {
        if(null== player) return;

        // Start buffering
        if (!wasRestored) {
            if (VIDEO_ID != ""){
                player.cueVideo(VIDEO_ID);}
        }

        player.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
            @Override
            public void onAdStarted() { }

            @Override
            public void onError(YouTubePlayer.ErrorReason arg0) { }

            @Override
            public void onLoaded(String arg0) {
                player.setPlayerStyle(YouTubePlayer.PlayerStyle.CHROMELESS);
                //player.play();
                }

            @Override
            public void onLoading() { }

            @Override
            public void onVideoEnded() {
                if(list.size() > 0){
                    currentPosition[0] = (currentPosition[0]+1) % list.size();
                    player.loadVideo(list.get(currentPosition[0]).GetVideoID());
                    player.play();

                }
            }

            @Override
            public void onVideoStarted() { }
        });

        playerHandle = player;
        st = new SeekThread(seekBar, playerHandle, list, currentPosition, this);
        st.start();



    }

    private YouTubePlayer playerHandle;
    private SeekThread st;
    private SeekBar seekBar;
    ArrayList<Item> list = new ArrayList<Item>();
    ListAdaptor itemAdapter;
    int[] currentPosition = {0};
    String SingerName = "";

    private void FillTheList(String SingerName){

        list = new ArrayList<Item>();
        Thread wt = new WikiGraberThread(SingerName, list, this);
        wt.start();
//        try {
//            wt.join();
//        }catch (Exception e){
//            Log.v("MainActivity",e.getMessage());
//        }


        itemAdapter = new ListAdaptor(this, list, currentPosition);
        final ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(itemAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentPosition[0] = position;
                playerHandle.loadVideo(list.get(currentPosition[0]).GetVideoID());
                playerHandle.play();
                //view.setBackgroundColor(ContextCompat.getColor(MainActivity.this ,R.color.playing));
                //getViewByPosition(position, listView).setBackgroundColor(ContextCompat.getColor(MainActivity.this ,R.color.playing));

            }
        });
    }

    public void UpdateListView(){
        itemAdapter.notifyDataSetChanged();
    }

    public View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition ) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }


}