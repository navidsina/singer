package com.example.android.singer;


import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.youtube.player.YouTubePlayer;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Created by sina on 1/21/2017.
 */

public class WikiGraberThread extends Thread {
    String SingerName;
    ArrayList<Item> list;
    MainActivity mainActivity;



    WikiGraberThread(String SingerName, ArrayList<Item> List, MainActivity mainActivity){
        this.SingerName = SingerName;
        this.list = List;
        this.mainActivity = mainActivity;
    }

    public void run(){
        String urlForWikipediaSearch = "";
        try {
            urlForWikipediaSearch = "https://en.wikipedia.org/w/api.php?action=opensearch&search=" + URLEncoder.encode(SingerName, "utf-8") + "&limit=1&namespace=0";
        }
        catch (Exception e)
        {
            Log.v("WikiGraberThread", e.getMessage());
        }

        String wikiSearchResult = HTMLContentGraber(urlForWikipediaSearch);
        String[] singerNameAndSingerWikiURL = GetSingerNameAndSingerWikiURL(wikiSearchResult);

        String pageContent = HTMLContentGraber(singerNameAndSingerWikiURL[1]);

        ArrayList<ArrayList<String>> albums = GetAlbumTitlesAndAlbumHrefs(pageContent);
        ArrayList<String> songs = new ArrayList<String>();
        for(int i = 0; i < albums.size(); ++i){
            String albumPageContent = HTMLContentGraber("https://en.wikipedia.org" + albums.get(i).get(0));
            songs = GetAlbumSongs(albumPageContent);
            for(int j = 0; j < songs.size(); ++j){
                try{
                    String youtubeSearchURL = "https://www.googleapis.com/youtube/v3/search?maxResults=1&part=snippet&" + "q="
                            + URLEncoder.encode(singerNameAndSingerWikiURL[0], "utf-8") + "+" + URLEncoder.encode(albums.get(i).get(1), "utf-8") + "+" + URLEncoder.encode(songs.get(j), "utf-8") + "&type=video&key=AIzaSyCnDXQ09rAcvuOdwcw4m0KMlRrUBKDa6Qc";
                    String youtubeSearchResult = HTMLContentGraber(youtubeSearchURL);
                    int helpingIndex1 = youtubeSearchResult.indexOf("\"videoId\": ") + 12;
                    int helpingIndex2 = youtubeSearchResult.indexOf("\"", helpingIndex1);
                    String videoID = youtubeSearchResult.substring(helpingIndex1,helpingIndex2);
                    helpingIndex1 = youtubeSearchResult.indexOf("high", helpingIndex1);
                    helpingIndex1 = youtubeSearchResult.indexOf("url", helpingIndex1) + 7;
                    helpingIndex2 = youtubeSearchResult.indexOf("\"", helpingIndex1);
                    String imageURL = youtubeSearchResult.substring(helpingIndex1, helpingIndex2);
                    list.add(new Item(albums.get(i).get(1), songs.get(j), videoID, imageURL));

                    int b = 99;
                    b = b+1;

                }catch (Exception e){
                    Log.v("WikiGrabberThread", e.getMessage());
                }
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mainActivity.UpdateListView();
                    }
                });
            }

        }




        int a = 9;
        a = a+1;
    }

    private String HTMLContentGraber(String URL){
        String pageContent = "";
        try{
            URL url = new URL(URL);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            pageContent = readStream(in);
            urlConnection.disconnect();
        }
        catch (Exception e){
            Log.v("WikiGrabberThread",e.getMessage());
        }
        return pageContent;
    }

    private String readStream(InputStream is) {
        try {
            String fileContent = "";
            byte[] part = new byte[1024];
            //ByteArrayOutputStream bo = new ByteArrayOutputStream();
            int numberOfBytesRead = is.read(part);
            while(numberOfBytesRead != -1) {
                //bo.write(i);
                //i = is.read();
                fileContent += new String(part, 0, numberOfBytesRead);
                numberOfBytesRead = is.read(part);
            }
            //return bo.toString();
            return fileContent;
        } catch (Exception e) {
            return "";
        }
    }

    private ArrayList<ArrayList<String>> GetAlbumTitlesAndAlbumHrefs(String PageContent){
        ArrayList<ArrayList<String>> albums = new ArrayList<ArrayList<String>>();
        int helpingIndex1, helpingIndex2, helpingIndex3, helpingIndex4, helpingIndex5, helpingIndex6;
        helpingIndex1 = PageContent.indexOf("Discography</span></h2>") + 23;
        while (true){
            helpingIndex2 = PageContent.indexOf("<li><i><a href=", helpingIndex1) + 16;
            helpingIndex3 = PageContent.indexOf("\"", helpingIndex2);
            String albumHref = PageContent.substring(helpingIndex2, helpingIndex3);
            helpingIndex4 = PageContent.indexOf(">", helpingIndex3) + 1;
            helpingIndex5 = PageContent.indexOf("<", helpingIndex4);
            String albumTitle = PageContent.substring(helpingIndex4, helpingIndex5);
            albums.add(new ArrayList<String>(asList(albumHref, albumTitle)));
            helpingIndex6 = PageContent.indexOf("</li>", helpingIndex5) + 6;
            if(PageContent.substring(helpingIndex6, helpingIndex6 + 5).equalsIgnoreCase("</ul>"))
                break;
            helpingIndex1 = helpingIndex5;
        }
        return albums;
    }

    private ArrayList<String> GetAlbumSongs(String AlbumPageContent){
        ArrayList<String> songs = new ArrayList<String>();
        String SongName = "";
        int helpingIndex1, helpingIndex2;
        helpingIndex1 = AlbumPageContent.indexOf("Track listing</span><span") + 25;
        helpingIndex1 = AlbumPageContent.indexOf("1.</td>", helpingIndex1);
//        helpingIndex1 = AlbumPageContent.indexOf("<", helpingIndex1) + 1;
//        if(AlbumPageContent.substring(helpingIndex1,helpingIndex1+6).equalsIgnoreCase("a href")) {
//            helpingIndex1 = AlbumPageContent.indexOf(" title=", helpingIndex1) + 8;
//            helpingIndex1 = AlbumPageContent.indexOf(">", helpingIndex1) + 1;
//            helpingIndex2 = AlbumPageContent.indexOf("<", helpingIndex1);
//        }
//        else {
//            helpingIndex1 = AlbumPageContent.indexOf(">", helpingIndex1) + 1;
//            helpingIndex2 = AlbumPageContent.indexOf("\"", helpingIndex1);
//        }
//        SongName = AlbumPageContent.substring(helpingIndex1, helpingIndex2);
//        songs.add(SongName);

        while (true){
            helpingIndex1 = AlbumPageContent.indexOf(".</td>", helpingIndex1) + 9;
            helpingIndex2 = AlbumPageContent.indexOf(">", helpingIndex1 - 15) + 1;
            try {
                if(Integer.parseInt(AlbumPageContent.substring(helpingIndex2, helpingIndex1 - 9)) != (songs.size()+1))
                    break;
            } catch (Exception e){
                break;
            }
            helpingIndex1 = AlbumPageContent.indexOf(">", helpingIndex1) + 2;
            String tt = AlbumPageContent.substring(helpingIndex1,helpingIndex1+6);
            if(AlbumPageContent.substring(helpingIndex1,helpingIndex1+6).equalsIgnoreCase("<a hre")) {
                helpingIndex1 = AlbumPageContent.indexOf(" title=", helpingIndex1) + 8;
                helpingIndex1 = AlbumPageContent.indexOf(">", helpingIndex1) + 1;
                helpingIndex2 = AlbumPageContent.indexOf("<", helpingIndex1);
            }
            else {
                helpingIndex2 = AlbumPageContent.indexOf("\"", helpingIndex1);
            }
            SongName = AlbumPageContent.substring(helpingIndex1, helpingIndex2);
            songs.add(SongName);

        }
        return songs;
    }

    private String[] GetSingerNameAndSingerWikiURL(String WikiSreachResult){
        int helpingIndex1 = WikiSreachResult.indexOf(",[\"") + 3;
        int helpingIndex2 = WikiSreachResult.indexOf("\"]", helpingIndex1);
        String singerName = WikiSreachResult.substring(helpingIndex1, helpingIndex2);
        helpingIndex1 = WikiSreachResult.indexOf("http", helpingIndex2);
        helpingIndex2 = WikiSreachResult.indexOf("\"", helpingIndex1);
        String SingerWikiURL = WikiSreachResult.substring(helpingIndex1, helpingIndex2);
        String[] returnValue = {singerName, SingerWikiURL};
        return returnValue;
    }
}
