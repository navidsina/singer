package com.example.android.singer;

import android.media.MediaPlayer;
import android.util.Log;
import android.widget.SeekBar;

import com.google.android.youtube.player.YouTubePlayer;

import java.util.ArrayList;

/**
 * Created by sina on 1/20/2017.
 */

public class SeekThread extends Thread{
    SeekBar seekBar;
    com.google.android.youtube.player.YouTubePlayer youTubePlayer;
    ArrayList<Item> list;
    int[] currentlyPlaying;
    MainActivity mainActivity;
    SeekThread(SeekBar seekBar, com.google.android.youtube.player.YouTubePlayer youTubePlayer, ArrayList<Item> list, int[] currentlyPlaying, MainActivity mainActivity) {
        this.seekBar = seekBar;
        this.youTubePlayer = youTubePlayer;
        this.list = list;
        this.currentlyPlaying = currentlyPlaying;
        this.mainActivity = mainActivity;
    }

    public void run(){
        try {
            while (true) {
                int duration = youTubePlayer.getDurationMillis();
                if(duration != 0){
                    int progress = 100 * youTubePlayer.getCurrentTimeMillis() / youTubePlayer.getDurationMillis();
                    seekBar.setProgress(progress);
                    if(list.size()>currentlyPlaying[0]){
                        list.get(currentlyPlaying[0]).SetCurrentlyPlaying(true);
                    }
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mainActivity.UpdateListView();
                        }
                    });
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
        }catch (Exception e)
        {
            Log.v("errorst",e.getMessage());

        }

    }
}